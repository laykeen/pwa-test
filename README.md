# PWA_test

Using this repo for various web dev tests

## How to try it

Visit this repo page!

### Used technologies

- generated logo: https://components.ai/logo/

- maskable icons: https://css-tricks.com/maskable-icons-android-adaptive-icons-for-your-pwa/

- web manifest: https://developer.mozilla.org/en-US/docs/Web/Manifest

- service workers: https://developers.google.com/web/fundamentals/primers/service-workers

- React: https://reactjs.org/ (TODO)
