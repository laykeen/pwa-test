if('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
        navigator.serviceWorker.register('/lib/scripts/sw.js').then(
            (registration) => {
                console.log("SW registered! ", registration.scope);
            },
            (err) => {
                console.warn("Error on registering SW! ", err);
            }
        );
    });
}
