// Here resides the service worker

console.log("Service worker here");

var CACHE_NAME = 'PWAtest_cache'+SW_VERSION;
var urls_to_cache - [
    '/',
    '/styles/main.css',
    '/scripts/main.js'
];

// https://developers.google.com/web/fundamentals/primers/service-workers

self.addEventListener('install', (event) => {
    event.waitUntil(
        //open cache
        caches.open(CACHE_NAME).then((cache) => {
            console.log("Cache opened");
            //cache files
            return cache.addAll(urls_to_cache);
        })
    );
});

self.addEventListener('fetch', (event) => {
    event.respondWith(
        caches.match(event.request).then((response) => {
            if(response)
                return response;

            return fetch(event.request).then((response) => {
                if(!response || response !== 200 || response !== 'basic')
                    return response;

                //clone the response to cache it
                var response_to_cache = respone.clone();

                cache.open(CACHE_NAME).then((cache) => {
                    cache.put(event.request, response_to_cache);
                });

                return response;
            });
        })
    );
});
